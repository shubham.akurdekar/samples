const https = require('https');
const fs = require('fs');

const options = {
  key: fs.readFileSync('./security/cert.key'),
  cert: fs.readFileSync('./security/cert.pem')
};

const server = https.createServer(options, (req, res) => {
  if (req.method === 'GET' && req.url === '/') {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello, World!');
  } else {
    res.writeHead(404);
    res.end();
  }
});

server.listen(5000, () => {
  console.log('Server listening on port 5000');
});
